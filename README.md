# Aprendiendo con Coursera

Bienvenido a mi proyecto web con Nodejs que aprendo con Coursera y la Universidad Austral.
## Cómo instalar **Paso a paso**

Todos estamos aprendiendo y quiero que quienes revisen este proyecto no tengan dificultades o tengan que buscar recursos externos para hacerlo.
Recomiendo utilizar el *Git Bash* para realizar estos comandos, aunque la línea de comandos de windows es suficiente.

**Primer paso** clona este repositorio en tu equipo local con el comando *git clone https://bitbucket.org/ArpadMrFlan/guia-hoteles-arpad/src/master/*

**Segundo paso** Instala las dependecias y paquetes requeridos del package.json con el comando *npm install* o *nmp install package.json*

**Tercer paso** Ejecuta el proyecto con el comando *npm run dev*

Espero esta guía, a pesar de ser redundante, haya sido de ayuda.